import React,{FC} from 'react'
import styled, { ThemeProvider } from 'styled-components'

interface ButtonProps {
    type:string
}
interface ButtonBaseProps {
    type:string;
    theme:string
}

const ButtonBase:FC = styled.button<ButtonBaseProps>`
    font-size:1em;
    padding:0.25em 1.5em;
    border-radius: 1em;
    width:fit-content;
    border: 1px solid ${props => props.theme[props.type].color};
    color:${props => props.theme[props.type].color};
    background:${props => props.theme[props.type].bg}
`

// ButtonBase.defaultProps = {
//     theme: {
//         main: '#4E94FB',
//         mainBackground: '#fff'
//     }
// }

const theme = {
    primary: {
        color: '#4E94FB',
        bg: '#fff'
    },
    dark: {
        color: '#fff',
        bg: '#5089DD'
    }
}

const Button:FC<ButtonProps> = (props) => {
    console.log('props:', props)
    return <ThemeProvider theme={theme}>
        <ButtonBase {...props}></ButtonBase>
    </ThemeProvider>

}

export {
    Button
}