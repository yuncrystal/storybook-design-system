import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./store";
import { Button } from 'sharedComponents/Buttons'
import { useAppDispatch } from "./store";
import { fetchTodoList } from './slices'

const Container = () => {
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchTodoList());
    }, []);


    return <div>
        <Button type={'dark'}>Normal Button</Button>
    </div>
}

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Container />
        </Provider>
    </React.StrictMode>,
    document.getElementById("root")
);



