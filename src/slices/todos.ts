import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchTodos } from "../apis";
import { initialTodosState } from "../types";


const initialState: initialTodosState = {
    entities: {
    },
    loading: "idle",
};

const fetchTodoList = createAsyncThunk(
    "symbols/fetchTodoList",
    async (_, { }) => {
        const response = await fetchTodos();
        return response;
    }
);

const todoSlice = createSlice({
    name: "symbols",
    initialState,
    reducers: {

    },
    extraReducers: (builder) => {
        builder.addCase(fetchTodoList.pending, (state, action) => {
            console.log("extraReducers");
        });
    },
});


export {
    fetchTodoList
}
// export const { symbolsFetch, getDefaultSymbols } = symbolsSlice.actions;
export default todoSlice.reducer;
