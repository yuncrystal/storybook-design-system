import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import commonjs from "@rollup/plugin-commonjs";
import typescript from "rollup-plugin-typescript2";
import image from "@rollup/plugin-image";

export default {
  input: [
    "src/index.ts",
    "src/stripbarPanel/index.ts"
  ],
  output: [
    {
      dir: 'dist',
      format: 'cjs',
      sourcemap: true
    }
  ],
  preserveModules: true,
  external: ['tslib'],
  plugins: [
    peerDepsExternal(),
    commonjs(),
    image(),
    typescript({ useTsconfigDeclarationDir: true }),
  ]
}
