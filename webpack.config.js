const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require("webpack");

module.exports = (env) => {
  // console.log('env.file:',env.file)
  // console.log('env.type:',env.type)
  return {
    mode: "development",
    entry: './src/index.tsx',
    output: {
      path: path.resolve(__dirname, "./dist"),
      filename: "[name].bundle.[contenthash].js",
    },
    devServer: {
      historyApiFallback: true,
      open: true,
      compress: true,
      hot: true,
      port: 8080,
    },
    module: {
      rules: [
        {
          test: /\.js?|.tsx?|.ts?$/,
          exclude: /(node_modules)/,
          use: {
            loader: "babel-loader",
          },
        },
        {
          test: /\.tsx?|.ts?$/,
          exclude: /(node_modules)/,
          use: {
            loader: "ts-loader",
          },
        },
      ],
    },
    devtool: "inline-source-map",
    resolve: {
      modules: [path.resolve(__dirname, "src"), "node_modules"],
      extensions: ["*", ".tsx", ".ts", ".js", ".jsx"],
      mainFiles: ["index"],
     
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        title: "webpack Boilerplate",
        template: path.resolve(__dirname, "./src/index.html"), // template file
        filename: "index.html", // output file
      }),
    ],
  };
};
